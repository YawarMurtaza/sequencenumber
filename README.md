  public class Employee
    {
        public int Id { get; set; }

        public int SequenceNumber { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTime DateOfBirth { get; set; }
    }
	
	
	
	////// Data Access Layer
	
	 public interface IEmployeeDataAccess
    {
        IEnumerable<Employee> GetAllEmployees();
    }



 public class EmployeeDataAccess : IEmployeeDataAccess
    {
        private readonly Database employeeDb;


        public EmployeeDataAccess()
        {
            DatabaseProviderFactory dbFact = new DatabaseProviderFactory();
            this.employeeDb = dbFact.Create("EMPLOYEE_DB_CONNECTION_STRING");
            // this.employeeDb = DatabaseFactory.CreateDatabase("EMPLOYEE_DB_CONNECTION_STRING");
        }

        public IEnumerable<Employee> GetAllEmployees()
        {

            using (DbCommand command = this.employeeDb.GetStoredProcCommand("dbo.spGetAllEmpoyees"))
            {
                using (IDataReader reader = this.employeeDb.ExecuteReader(command))
                {
                    while (reader.Read())
                    {
                        yield return new Employee()
                        {
                            DateOfBirth = DateTime.Parse(reader["DateOfBirth"].ToString()),
                            Id = int.Parse(reader["Id"].ToString()),
                            LastName = reader["LastName"].ToString(),
                            FirstName = reader["FirstName"].ToString(),
                            SequenceNumber = int.Parse(reader["SequenceNumber"].ToString())
                        };
                    }
                }
            }
        }
    }
	
	
	/// this is your winfoem 
	public partial class Form1 : Form
    {
        IEmployeeDataAccess dataAccess;

        public Form1()
        {
            InitializeComponent();
            this.dataAccess = new EmployeeDataAccess();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            foreach (Employee nextEmp in this.dataAccess.GetAllEmployees())
            {
                this.employeeGrid.Rows.Add(nextEmp.Id, nextEmp.FirstName, nextEmp.LastName, nextEmp.DateOfBirth, nextEmp.SequenceNumber);
            }
            
        }
    }